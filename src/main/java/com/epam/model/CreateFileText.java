package com.epam.model;

import java.io.*;

public class CreateFileText implements AutoCloseable {
    private PrintWriter pw;
    private static int countFile = 0;
    private String name = "TextFile";


    public CreateFileText() {
        countFile++;
        this.name = name;
        try {
            System.out.println("Created file");
            pw = new PrintWriter(name + countFile + ".txt", "UTF-8");
            pw.println("New file" + this.name);
            pw.write("Text:" + this.name + countFile);
            pw.append("SSD");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            System.out.println("File not created");
        }
        pw.println("Text");
    }

    @Override
    public void close() throws Exception {
        if (countFile == 3) {
            throw new IOException("Exception from file 3");
        }
        if (pw != null) {
            pw.println("Except");
            pw.close();
        }
    }
}
